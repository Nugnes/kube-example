apt update

# docker
apt install -y docker.io


systemctl start docker
systemctl enable docker

#kubernetes
apt install -y apt-transport-https curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add

apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

apt install -y kubeadm kubelet kubectl kubernetes-cni

#disable swap
sudo swapoff -a

sed -i '/swap/d' /etc/fstab


#set hostname
hostnamectl set-hostname node2.local


#init kubectl
#kubeadm init

# connect 
sudo kubeadm join 192.168.0.1:6443 --token nvzivu.all2n3lkmzs0wght --discovery-token-unsafe-skip-ca-verification

# set user for kubectl
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/kubelet.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

