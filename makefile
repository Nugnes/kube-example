up: up_node1 up_node2
	echo up

up_node1:
	vagrant up node1

up_node2:
	vagrant up node2


halt: halt_node2 halt_node1
	echo "halt in progress"

halt_node1:
	vagrant halt node1

halt_node2:
	vagrant halt node2

destroy:
	vagrant destroy -f
