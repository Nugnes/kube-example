apt update

# docker
apt install -y docker.io


systemctl start docker
systemctl enable docker

#kubernetes
apt install -y apt-transport-https curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add

apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

apt install -y kubeadm kubelet kubectl kubernetes-cni

#disable swap
sudo swapoff -a

sed -i '/swap/d' /etc/fstab


# set hostname
hostnamectl set-hostname node1.local


# init kubectl
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-cert-extra-sans 192.168.0.1 --service-dns-domain node1.local --apiserver-advertise-address 192.168.0.1 --token nvzivu.all2n3lkmzs0wght


# set user for kubectl
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


# deploy network
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml

# remove taint on node
kubectl taint nodes --all node-role.kubernetes.io/master-
